/* eslint-disable jsx-a11y/no-onchange */

// External dependencies
import React, { useEffect, useMemo, useState } from 'react';

// Project dependencies
import useFCCWeather from '../../hooks/useFCCWeather.js';
import {
	convertTemperature,
	getUnitSymbol
} from '../../utils/temperature.js';

// Internal dependencies
import './style.css';

export default function WeatherInfo() {
	const [ weatherInfo, fetchAndSetWeatherInfo ] = useFCCWeather();
	const [ temperatureUnit, setTemperatureUnit ] = useState( 'celsius' );
	const displayedTemperature = useMemo(
		() => {
			if ( weatherInfo.hasOwnProperty( 'temperature' ) ) {
				return (
					String( convertTemperature( weatherInfo.temperature, 'celsius', temperatureUnit ) ) +
					getUnitSymbol( temperatureUnit )
				);
			}
		},
		[ weatherInfo, temperatureUnit ]
	);

	useEffect(
		() => {
			fetchAndSetWeatherInfo();
		},
		[]
	);

	function handleSelectTemperatureUnit( event ) {
		// Get selected unit from HTML select element.
		const selectedUnit = event.target.selectedOptions[0].value;
		setTemperatureUnit( selectedUnit );
	}

	function generateDisplayedErrorMessage() {
		if ( weatherInfo.hasOwnProperty( 'error' ) ) {
			const { error } = weatherInfo;

			if ( weatherInfo.error.hasOwnProperty( 'name' ) ) {
				switch ( error.name ) {
					case 'IncompatibleBrowserError':
						return <p>Your browser does not support geolocation or is out-of-date. Please update your browser and operating system. I recommend the Dissenter browser and Kubuntu Linux operating system.</p>;
					case 'PositionError':
						return <p>Failed to retrieve geolocation data. PositionError: { error.message }</p>;
					case 'WeatherAPIError':
						return <p>Weather API returned an error: { error }</p>;
					default:
						return <p>An unknown error occurred. { error.name }: { error.message }</p>;
				}
			} else {
				return <p>An unknown error occurred: { error.message }</p>;
			}
		}
	}

	return (
		<div className="zwd-weather-info">
			{
				( ! weatherInfo.hasOwnProperty( 'temperature' ) ) &&
				( ! weatherInfo.hasOwnProperty( 'error' ) ) &&
				<p>Loading weather info…</p>
			}
			{
				weatherInfo.hasOwnProperty( 'temperature' ) &&
				<>
					<p className="zwd-weather-info__opening-sentence">
						The weather in { weatherInfo.location } is:
					</p>
					<div className="zwd-weather-info__general-weather">
						<img
							className="zwd-general-weather__general-weather-icon"
							src={ weatherInfo.generalWeatherIconSrc }
							alt=""
						/>
						<p className="zwd-general-weather__general-weather-description">
							{ weatherInfo.generalWeather }
						</p>
					</div>
					<p className="zwd-general-weather__temperature">
						Temperature:  { displayedTemperature }
					</p>
				</>
			}
			{
				weatherInfo.hasOwnProperty( 'error' ) && generateDisplayedErrorMessage()
			}
			<div className="zwd-general-weather__controls">
				<button
					className="zwd-general-weather__button"
					onClick={ fetchAndSetWeatherInfo }
					type="button"
				>
					Refresh Stats
				</button>
				<select
					className="zwd-general-weather__temperature-unit-select"
					name="zwd-temperature-unit-select"
					onChange={ handleSelectTemperatureUnit }
				>
					<option value="celsius">Celsius</option>
					<option value="fahrenheit">Fahrenheit</option>
					<option value="kelvin">Kelvin</option>
				</select>
			</div>
		</div>
	);
}