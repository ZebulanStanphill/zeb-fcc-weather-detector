// External dependencies
import React from 'react';

// Internal dependencies
import './style.css';

export default function Header() {
	return (
		<header className="zwd-header">
			<h1 className="zwd-header__h1">Zeb’s Weather Detector</h1>
		</header>
	);
}