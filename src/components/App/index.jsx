// External dependencies
import React from 'react';

// Project component dependencies
import Header from '../Header/index.jsx';
import Footer from '../Footer/index.jsx';
import WeatherInfo from '../WeatherInfo/index.jsx';

// Internal dependencies
import './style.css';

export default function App() {
	return (
		<div className="zwd">
			<Header />
			<WeatherInfo />
			<Footer />
		</div>
	);
}