export const shiftNumberStringArray = ( numArray, precision ) =>
	+( numArray[0] + 'e' + ( numArray[1] ? ( +numArray[1] + precision ) : precision ) );

export const shift = ( number, precision ) =>
	shiftNumberStringArray( number.toString().split( 'e' ), precision );

// Better round function. Based off of example here:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round#A_better_solution
export const round = ( number, precision ) =>
	shift( Math.round( shift( number, precision ) ), -precision );