import { round } from './math.js';

export const fahrenheitToCelsius = fahrenheitTemperature =>
	round( ( fahrenheitTemperature - 32 ) * 5 / 9, 2 );

export const kelvinToCelsius = kelvinTemperature =>
	round( kelvinTemperature - 273.15, 2 );

export const celsiusToFahrenheit = celsiusTemperature =>
	round( celsiusTemperature * 9 / 5 + 32, 2 );

export const kelvinToFahrenheit = kelvinTemperature =>
	round( kelvinTemperature * 9 / 5 - 459.67, 2 );

export const celsiusToKelvin = celsiusTemperature =>
	/* Because floating point math is funky, I have to multiply and divide by 10 to get
	proper results. */
	round( ( celsiusTemperature * 10 + 273.15 * 10 ) / 10, 2 );

export const fahrenheitToKelvin = fahrenheitTemperature =>
	/* Because floating point math is funky, I have to multiply and divide by 10 to get
	proper results. */
	round( ( fahrenheitTemperature * 10 + 459.67 * 10 ) / 10 * 5 / 9, 2 );

export const toCelsius = ( temperature, convertFrom ) =>
	convertFrom == 'kelvin' ? kelvinToCelsius( temperature ) :
		convertFrom == 'fahrenheit' ? fahrenheitToCelsius( temperature ) :
			temperature; // TODO: Update to throw errors when unrecognized temperature unit.

export const toFahrenheit = ( temperature, convertFrom ) =>
	convertFrom == 'celsius' ? celsiusToFahrenheit( temperature ) :
		convertFrom == 'kelvin' ? kelvinToFahrenheit( temperature ) :
			temperature; // TODO: Update to throw errors when unrecognized temperature unit.

export const toKelvin = ( temperature, convertFrom ) =>
	convertFrom == 'celsius' ? celsiusToKelvin( temperature ) :
		convertFrom == 'fahrenheit' ? fahrenheitToKelvin( temperature ) :
			temperature; // TODO: Update to throw errors when unrecognized temperature unit.

export const convertTemperature = ( temperature, convertFrom, convertTo ) =>
	convertTo == 'celsius' ? toCelsius( temperature, convertFrom ) :
		convertTo == 'fahrenheit' ? toFahrenheit( temperature, convertFrom ) :
			convertTo == 'kelvin' ? toKelvin( temperature, convertFrom ) :
				temperature; // TODO: Update to throw errors when unrecognized temperature unit.

export const getUnitSymbol = unit =>
	unit == 'kelvin' ? 'K' :
		unit == 'celsius' ? '°C' :
			unit == 'fahrenheit' ? '°F' :
				unit; // Fallback to just displaying full unit name.