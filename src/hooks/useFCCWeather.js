// External dependencies
import { useState } from 'react';

// Project dependencies
import { round } from '../utils/math.js';

/*
Promise wrapper function for navigator.geolocation.getCurrentPosition.

navigator.geolocation.getCurrentPosition is an old-fashioned async function
that uses callback functions passed as arguments to it. Wrapping this function
in a Promise lets you use the function with the async and await JS keywords,
allowing for async code that fits in with synchronous code and avoids "callback
hell".

This function also adds a name property to errors thrown by getCurrentPosition,
so that the errors can bemore easily identified.

MDN guide to using promises:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
*/
const getPosition = options =>
	new Promise( ( resolve, reject ) => {

		const success = position => resolve( position );

		// Add name to errors returned from getCurrentPosition.
		// See also: https://stackoverflow.com/questions/44427908/catch-geolocation-error-async-await/44439358#44439358
		const failure = ( { code, message } ) =>
			reject(
				Object.assign(
					new Error( message ),
					{ code, name: 'PositionError' }
				)
			);

		// The standard JS async function that we are wrapping in a promise.
		navigator.geolocation.getCurrentPosition( success, failure, options );
	} );

// Make API request to freeCodeCamp weather API.
const freeCodeCampWeatherAPIRequest = ( latitude, longitude ) =>
	fetch(
		`https://fcc-weather-api.glitch.me/api/current?lat=${latitude}&lon=${longitude}`
	);

// StackOverflow answer explaining 3 ways to handle async functions: https://stackoverflow.com/questions/14220321/how-do-i-return-the-response-from-an-asynchronous-call/14220323#14220323
// async/await docs: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
// Blog post explaining async/await: https://hackernoon.com/6-reasons-why-javascripts-async-await-blows-promises-away-tutorial-c7ec10518dd9
// Warning not to abuse await: https://medium.freecodecamp.org/avoiding-the-async-await-hell-c77a0fb71c4c
async function getWeather( position ) {
	const { latitude, longitude } = position.coords;

	// Use freeCodeCamp weather API to obtain weather info as JSON.
	return ( await freeCodeCampWeatherAPIRequest( latitude, longitude ) ).json();
}

// Custom hook for using weather info from freeCodeCamp Glitch API.
export default function useFCCWeather() {
	const [ weatherInfo, setWeatherInfo ] = useState( {} );

	async function fetchAndSetWeatherInfo() {
		try {
			if ( navigator.geolocation ) { // If geolocation services are available
				// Try to get position using geolocation API.
				// https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation
				const weatherAPIJSON = await getWeather( await getPosition() );

				// TODO: add comment here explaining why this if statement is needed.
				if ( weatherAPIJSON.hasOwnProperty( 'weather' ) ) {
					// eslint-disable-next-line no-console
					console.log( weatherAPIJSON );

					setWeatherInfo( {
						location: weatherAPIJSON.name,
						generalWeather: weatherAPIJSON.weather[0].main,
						generalWeatherIconSrc: weatherAPIJSON.weather[0].icon,
						temperature: round( weatherAPIJSON.main.temp, 2 )
					} );
				} else if ( weatherAPIJSON.hasOwnProperty( 'error' ) ) {
					const weatherAPIError = new Error( weatherAPIJSON.error );

					weatherAPIError.name = 'WeatherAPIError';

					throw weatherAPIError;
				} else {
					throw new Error( 'An unknown error occurred.' );
				}
			} else {
				const incompatibleBrowserError = new Error( 'Browser does not support geolocation. Please update your browser.' );

				incompatibleBrowserError.name = 'IncompatibleBrowserError';

				throw incompatibleBrowserError;
			}
		} catch ( error ) {
			setWeatherInfo( {
				error: error
			} );
		}
	}

	return [ weatherInfo, fetchAndSetWeatherInfo ];
}